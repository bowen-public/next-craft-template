import fs from "fs";
import { CmdFnParams } from ".";
import { capitalizeFirst, capitalizeFirstUnderscore } from "./util";

export default async function createComponentBlocks(params: CmdFnParams) {
  const { types, sliceBlocksName } = params;
  const blockUnion = types.find(
    (t) => t.name === `${sliceBlocksName}_MatrixField`
  );
  if (blockUnion && blockUnion.possibleTypes) {
    let fc = `// !!!!! Auto Generated do NOT Edit !!!!!

import { FC } from "react";
import * as Q from "generated/queries";
import * as C from "./Blocks";

export type ComponentBlockUnion =
  | null
  ${blockUnion.possibleTypes
    .map((pt) => `| Q.${capitalizeFirstUnderscore(pt.name)}Fragment`)
    .join("\n  ")}

export type ComponentBlocksProps = {
  componentBlocks: ComponentBlockUnion[]
};

const ComponentBlocks: FC<ComponentBlocksProps> = ({ componentBlocks }) => {
  return (
    <div>
      {componentBlocks.map((block, i) => {
        switch (block?.__typename) {
          ${blockUnion.possibleTypes
            .map((pt) => {
              const { name } = pt;
              const [, cname] = name.split("_");
              return `case "${name}":
            return <C.${capitalizeFirst(cname)} key={i} {...block} />;`;
            })
            .join("\n          ")}
          default:
            return null;
        }
      })}
    </div>
  );
};

export default ComponentBlocks;    
`;

    fs.writeFileSync(`${params.componentsPath}/ComponentBlocks.tsx`, fc);

    let fc2 = "// !!!!! Auto Generated do NOT Edit !!!!!\n\n";
    fc2 += blockUnion.possibleTypes
      .map((pt) => {
        const { name } = pt;
        const [, cname] = name.split("_");
        return `export { default as ${capitalizeFirst(
          cname
        )} } from "./${capitalizeFirst(cname)}";`;
      })
      .join("\n");
    fc2 += "\n";
    fs.writeFileSync(`${params.componentsPath}/Blocks/index.ts`, fc2);

    blockUnion.possibleTypes.map((pt) => {
      const { name } = pt;
      const [, cname] = name.split("_");
      const cmpPath = `${params.componentsPath}/Blocks/${capitalizeFirst(
        cname
      )}.tsx`;
      let fc = `import * as Q from "generated/queries";
import { FC } from "react";


const ${capitalizeFirst(cname)}: FC<Q.${capitalizeFirstUnderscore(
        name
      )}Fragment> = (props) => {
  return <div></div>;
};

export default ${capitalizeFirst(cname)};      
    `;
      try {
        fs.statSync(cmpPath);
      } catch (err) {
        fs.writeFileSync(cmpPath, fc);
      }
    });
  }
}
