import fs from "fs";
import { CmdFnParams } from ".";
import {
  capitalizeFirst,
  isEntryUnion,
  isSectionFragment,
  parseEntryUnionName,
  parseSectionFragmentName,
  spaces,
  writeFields
} from "./util";

export default async function createEntryQueries(params: CmdFnParams) {
  const { types } = params;
  const sectionFragments = types.filter(isSectionFragment);
  const entryUnions = types.filter(isEntryUnion);
  let fc = "";

  entryUnions.forEach((union, i) => {
    const {
      components: [sectionHandle]
    } = parseEntryUnionName(union);

    const { possibleTypes } = union;

    if (possibleTypes && possibleTypes.length) {
      const [n] = possibleTypes;
      const parsedN = n.name.split("_");
      if (possibleTypes.length === 1 && parsedN[0] === parsedN[1]) {
        // IS A SINGLE
        fc += `# ${capitalizeFirst(sectionHandle)} - Single Page Query\n\n`;
        fc += `query ${capitalizeFirst(sectionHandle)} {`;
        fc += "\n" + spaces(2) + `${sectionHandle}Entries {`;
        fc += "\n" + spaces(4) + `...${n.name}` + "\n";
        fc += spaces(2) + "}\n";
        fc += "}\n\n";
      } else {
        // IS A STRUCTURE OR CHANNEL
        fc += `# ${capitalizeFirst(
          sectionHandle
        )} - Dynamic Urls Page Query\n\n`;
        fc += `query ${capitalizeFirst(sectionHandle)}($slug: [String]!) {`;
        fc += "\n" + spaces(2) + `${sectionHandle}Entries(slug: $slug) {`;
        fc += "\n" + spaces(4);
        fc += possibleTypes.map((pt) => `...${pt.name}`).join("\n" + spaces(4));
        fc += "\n" + spaces(2) + "}\n";
        fc += "}\n\n";

        fc += `# ${capitalizeFirst(sectionHandle)} - Paths Query\n\n`;
        fc += `query ${capitalizeFirst(sectionHandle)}Paths {`;
        fc += "\n" + spaces(2) + `${sectionHandle}Entries(limit: 100) {`;
        fc += "\n" + spaces(4) + `... on ${n.name}` + " {\n";
        fc += spaces(6) + "id" + "\n";
        fc += spaces(6) + "slug" + "\n";
        fc += spaces(4) + "}\n";
        fc += spaces(2) + "}\n";
        fc += "}\n\n";
      }
    }
  });

  sectionFragments.forEach((block, i) => {
    const { fullName } = parseSectionFragmentName(block);
    const { fields } = block;

    if (fields && fields.length) {
      fc += `# ${capitalizeFirst(fullName)}\n\n`;
      fc += `fragment ${fullName} on ${fullName} {
  ${writeFields(params, fields, "Entry", 2)}
}`;
      fc += i === sectionFragments.length - 1 ? "\n" : "\n\n";
    }
  });

  fs.writeFileSync(`${params.queriesPath}/_entryQueries.graphql`, fc);
}
