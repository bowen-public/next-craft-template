import fs from "fs";
import { CmdFnParams } from ".";
import {
  capitalizeFirst,
  isSuperTable,
  parseSuperTableName,
  writeFields
} from "./util";

export default async function createSuperTableFragments(params: CmdFnParams) {
  const { types } = params;
  const superTables = types.filter(isSuperTable);
  let fc = "";

  superTables.forEach((block, i) => {
    const {
      fullName,
      components: [blockName]
    } = parseSuperTableName(block);
    const { fields } = block;
    if (fields && fields.length) {
      fc += `# ${capitalizeFirst(fullName)} Super Table Interface\n\n`;
      fc += `fragment ${blockName}_SuperTable on ${fullName} {
  ${writeFields(params, fields, "Matrix", 2)}
}`;
      fc += i === superTables.length - 1 ? "\n" : "\n\n";
    }
  });

  fs.writeFileSync(`${params.queriesPath}/_superTableFragments.graphql`, fc);
}
