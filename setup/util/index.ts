import { SchemaType } from "../getSchemaData";

export function capitalizeFirst(word: string) {
  const firstLetter = word.charAt(0);
  const firstLetterCap = firstLetter.toUpperCase();
  const remainingLetters = word.slice(1);

  return firstLetterCap + remainingLetters;
}

export function isSuperTable(type: SchemaType) {
  const { components } = parseSuperTableName(type);

  return components.length === 2 && components[1] === "BlockType";
}

export function parseSuperTableName(type: SchemaType) {
  const fullName = type.name ?? "";

  return {
    fullName,
    components: fullName.split("_")
  };
}

export function isMatrix(type: SchemaType) {
  const { components } = parseMatrixName(type);

  return components.length === 3 && components[2] === "BlockType";
}

export function parseMatrixName(type: SchemaType) {
  const fullName = type.name ?? "";

  return {
    fullName,
    components: fullName.split("_")
  };
}

export function isSectionFragment(type: SchemaType) {
  const { components } = parseSectionFragmentName(type);

  return components.length === 3 && components[2] === "Entry";
}

export function parseSectionFragmentName(type: SchemaType) {
  const fullName = type.name ?? "";

  return {
    fullName,
    components: fullName.split("_")
  };
}

export function isGlobalSet(type: SchemaType) {
  const { components } = parseGlobalSetName(type);

  return components.length === 2 && components[1] === "GlobalSet";
}

export function parseGlobalSetName(type: SchemaType) {
  const fullName = type.name ?? "";

  return {
    fullName,
    components: fullName.split("_")
  };
}

export function isEntryUnion(type: SchemaType) {
  const { components } = parseEntryUnionName(type);

  return components.length === 2 && components[1] === "SectionEntryUnion";
}

export function parseEntryUnionName(type: SchemaType) {
  const fullName = type.name ?? "";

  return {
    fullName,
    components: fullName
      .split("SectionEntryUnion")
      .map((item, i) => (i === 1 && item === "" ? "SectionEntryUnion" : item))
  };
}

export function isAssetObject(type: SchemaType) {
  const { components } = parseAssetObject(type);

  return components.length === 2 && components[1] === "Asset";
}

export function parseAssetObject(type: SchemaType) {
  const fullName = type.name ?? "";

  return {
    fullName,
    components: fullName.split("_")
  };
}

export const assetFieldsName = "assetFields";

export function capitalizeFirstUnderscore(s: string) {
  return s.split("_").map(capitalizeFirst).join("_");
}

export function arg(arg: string): string | undefined {
  const customIndex = process.argv.indexOf(`--${arg}`);
  let argString;

  if (customIndex > -1) {
    argString = process.argv[customIndex + 1];
  }
  return argString;
}

export * from "./writeFields";
