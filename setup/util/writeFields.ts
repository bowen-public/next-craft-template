import { range } from "lodash";
import { assetFieldsName } from ".";
import { SchemaType, Field } from "../getSchemaData";
import { ignoreAsset, ignoreAll, ignoreEntry } from "../ignoreFields";
import { types } from "util";
import { CmdFnParams } from "..";

export type InterfaceType = "Asset" | "Entry" | "Matrix" | undefined;

export function spaces(n: number) {
  return range(0, n)
    .map((_) => " ")
    .join("");
}

export function writeFields(
  params: CmdFnParams,
  fields: Field[],
  iType: InterfaceType,
  indent: number
): string {
  const { types, entryOverrides } = params;

  return fields
    .map((f) => {
      const fieldName = f.name ?? "";
      const interfaceName = f.type.ofType?.ofType?.name;

      if (iType === "Asset" && ignoreAsset.includes(fieldName)) return null;
      if (iType === "Matrix" && ignoreAll.includes(fieldName)) return null;
      if (iType === "Entry" && ignoreEntry.includes(fieldName)) return null;
      if (f.type.kind === "SCALAR") {
        return fieldName;
      }
      if (interfaceName === "AssetInterface") {
        return `${fieldName} {\n${spaces(
          indent + 2
        )}...${assetFieldsName}\n${spaces(indent)}}`;
      }
      if (interfaceName === "EntryInterface" && entryOverrides) {
        const ors = entryOverrides.split(",");

        for (const or of ors) {
          const [fName, fragName] = or.split("-");
          if (fieldName === fName) {
            return `${fieldName} {\n${spaces(
              indent + 2
            )}...${fragName}\n${spaces(indent)}}`;
          }
        }
      }
      if (interfaceName === "EntryInterface") {
        return `${fieldName} {\n${spaces(indent + 2)}id\n${spaces(
          indent + 2
        )}title\n${spaces(indent + 2)}uri\n${spaces(indent)}}`;
      }
      if (f.type.name === "SeomaticInterface") {
        return `seomatic {\n${spaces(indent + 2)}metaTitleContainer\n${spaces(
          indent + 2
        )}metaTagContainer\n${spaces(indent + 2)}metaLinkContainer\n${spaces(
          indent
        )}}`;
      }
      if (interfaceName?.endsWith("_SuperTableField")) {
        return `${fieldName} {\n${spaces(
          indent + 2
        )}...${fieldName}_SuperTable\n${spaces(indent)}}`;
      }
      if (interfaceName?.endsWith("_MatrixField")) {
        const found = types.find((t) => t.name === interfaceName);
        if (found && found.possibleTypes) {
          return (
            `${fieldName} {\n${spaces(indent + 2)}` +
            found.possibleTypes
              .map((pt) => {
                return "..." + pt.name;
              })
              .join("\n" + spaces(indent + 2)) +
            "\n" +
            spaces(indent) +
            "}"
          );
        }
      }
      if (f.type.name === "linkField_Link") {
        return `${fieldName} {\n${spaces(indent + 2)}...linkFields\n${spaces(
          indent
        )}}`;
      }
      if (f.type.kind === "LIST" && f.type.ofType?.kind === "OBJECT") {
        const found = types.find(
          (t) =>
            fieldName.length &&
            t.kind === "OBJECT" &&
            t.name === `${fieldName}_TableRow`
        );

        if (found && found.fields) {
          return (
            `${fieldName} {\n${spaces(indent + 2)}` +
            writeFields(params, found.fields, undefined, 4) +
            "\n" +
            spaces(indent) +
            "}"
          );
        }
      }

      return null;
    })
    .filter((x) => x)
    .join("\n" + spaces(indent));
}
