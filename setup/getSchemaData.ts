import dotenv from "dotenv";
import fetch from "node-fetch-commonjs";

dotenv.config({ path: "./web/.env.local" });

export type SchemaType = {
  kind: string;
  name: string | null;
  fields: Field[] | null;
  possibleTypes: null | { name: string }[];
};

export type Field = {
  name: string | null;
  type: {
    kind: string;
    name: string | null;
    ofType: {
      kind: string;
      ofType: {
        kind: string;
        name: string | null;
      } | null;
    } | null;
  };
};

export type SchemaData = {
  data: {
    __schema: {
      types: SchemaType[];
    };
  };
};

export default async function getSchemaData() {
  const fetchUrl = process.env.API_CI_BUILD_URL ?? "";
  const authToken = process.env.CRAFT_GRAPHQL_AUTH_TOKEN ?? "";
  const res = await fetch(fetchUrl, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${authToken}`
    },
    body: JSON.stringify({
      variables: {},
      query: `
      {
        __schema {
          types {
            kind
            name
            possibleTypes {
              name
            }
            description
            fields {
              name
              type {
                kind
                name
                ofType {
                  kind
                  ofType {
                    kind
                    name
                  }
                }
              }
            }
          }
        }
      }      
    `
    })
  });
  const json = (await res.json()) as SchemaData;

  return json.data.__schema.types;
}
