import fs from "fs";
import { CmdFnParams } from ".";
import {
  capitalizeFirst,
  isMatrix,
  parseMatrixName,
  writeFields
} from "./util";

export default async function createMatrixFragments(params: CmdFnParams) {
  const { types } = params;
  const matrices = types.filter(isMatrix);
  let fc = "";

  matrices.forEach((block, i) => {
    const { fullName } = parseMatrixName(block);
    const { fields } = block;
    if (fields && fields.length) {
      fc += `# ${capitalizeFirst(fullName)}\n\n`;
      fc += `fragment ${fullName} on ${fullName} {
  ${writeFields(params, fields, "Matrix", 2)}
}`;
      fc += i === matrices.length - 1 ? "\n" : "\n\n";
    }
  });

  fs.writeFileSync(`${params.queriesPath}/_matrixFragments.graphql`, fc);
}
