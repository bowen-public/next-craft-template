export const ignoreMatrix = [
  "uid",
  "_count",
  "title",
  "slug",
  "uri",
  "enabled",
  "archived",
  "siteHandle",
  "siteId",
  "siteSettingsId",
  "language",
  "searchScore",
  "trashed",
  "status",
  "dateCreated",
  "dateUpdated",
  "fieldId",
  "primaryOwnerId",
  "typeId",
  "typeHandle",
  "sortOrder"
];

export const ignoreAsset = [
  "alt",
  "volumeId",
  "size",
  "img",
  "srcset",
  "format",
  "dateModified",
  "uid",
  "_count",
  "title",
  "slug",
  "uri",
  "enabled",
  "archived",
  "siteHandle",
  "siteId",
  "siteSettingsId",
  "language",
  "searchScore",
  "trashed",
  "status",
  "dateCreated",
  "dateUpdated"
];

export const ignoreEntry = [
  "uid",
  "_count",
  "enabled",
  "archived",
  "siteHandle",
  "siteId",
  "siteSettingsId",
  "language",
  "searchScore",
  "trashed",
  "status",
  "dateCreated",
  "dateUpdated",
  "fieldId",
  "primaryOwnerId",
  "typeId",
  "typeHandle",
  "sortOrder",
  "lft",
  "rgt",
  "level",
  "root",
  "structureId",
  "isDraft",
  "isRevision",
  "revisionId",
  "revisionNotes",
  "draftId",
  "isUnpublishedDraft",
  "draftName",
  "draftNotes",
  "canonicalId",
  "canonicalUid",
  "sectionId",
  "sectionHandle",
  "typeId",
  "typeHandle",
  "postDate",
  "expiryDate",
  "children",
  "descendants",
  "parent",
  "ancestors",
  "url",
  "localized",
  "prev",
  "next",
  // recent
  "seo",
  "enabledForSite"
];

export const ignoreAll = [
  ...ignoreMatrix,
  "lft",
  "rgt",
  "level",
  "root",
  "structureId",
  "isDraft",
  "isRevision",
  "revisionId",
  "revisionNotes",
  "draftId",
  "isUnpublishedDraft",
  "draftName",
  "draftNotes",
  "canonicalId",
  "canonicalUid",
  "sectionId",
  "sectionHandle",
  "typeId",
  "typeHandle",
  "postDate",
  "expiryDate",
  "children",
  "descendants",
  "parent",
  "ancestors",
  "url",
  "localized",
  "prev",
  "next"
];
