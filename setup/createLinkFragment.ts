import fs from "fs";
import { CmdFnParams } from ".";
import { writeFields } from "./util";

export default async function createLinkFragment(params: CmdFnParams) {
  const { types } = params;
  const linkFrag = types.find((t) => t.name === "linkField_Link");
  let fc = "";

  if (linkFrag) {
    const { fields } = linkFrag;
    if (fields && fields.length) {
      fc += `# Link Fields from plugin Typed Link Field\n\n`;
      fc += `fragment linkFields on linkField_Link {
  ${writeFields(params, fields, undefined, 2)}
}\n`;
    }
  }

  fs.writeFileSync(`${params.queriesPath}/_linkFieldsFragment.graphql`, fc);
}
