import fs from "fs";
import { CmdFnParams } from ".";
import {
  capitalizeFirst,
  isGlobalSet,
  parseGlobalSetName,
  spaces,
  writeFields
} from "./util";

export default async function createGlobalQueries(params: CmdFnParams) {
  const { types, additionalGlobal } = params;
  const globalFragments = types.filter(isGlobalSet);
  const globalSet = types.find((t) => t.name === "GlobalSetInterface");
  let fc = "";

  fc += "# Redirect" + "\n\n";
  fc += `query Redirect($uri: String) {
  retour(uri: $uri) {
    id
    redirectSrcUrl
    redirectDestUrl
    redirectSrcMatch
    redirectHttpCode
    redirectMatchType
    redirectSrcUrlParsed
  }
}\n\n`;

  fc += `# Global\n\n`;
  fc += `query Global {`;
  fc +=
    "\n" +
    spaces(2) +
    "seomatic {" +
    "\n" +
    spaces(4) +
    "metaScriptContainer" +
    "\n" +
    spaces(2) +
    "}";
  if (globalSet && globalSet.possibleTypes) {
    globalSet.possibleTypes.forEach((pt) => {
      const { name } = pt;
      const handle = name.split("_")[0];

      fc += "\n" + spaces(2) + `${handle}: globalSet(handle: "${handle}") {`;
      fc += "\n" + spaces(4) + `...${name}` + "\n";
      fc += spaces(2) + "}";
    });
  }
  const ors = additionalGlobal?.split(",");
  if (ors && ors.length) {
    ors.forEach((or) => {
      fc += "\n  ";
      fc += or;
    });
  }

  fc += "\n}\n\n";

  globalFragments.forEach((block, i) => {
    const { fullName } = parseGlobalSetName(block);
    const { fields } = block;

    if (fields && fields.length) {
      fc += `# ${capitalizeFirst(fullName)}\n\n`;
      fc += `fragment ${fullName} on ${fullName} {
  ${writeFields(params, fields, "Matrix", 2)}
}`;
      fc += i === globalFragments.length - 1 ? "\n" : "\n\n";
    }
  });

  fs.writeFileSync(`${params.queriesPath}/_globals.graphql`, fc);
}
