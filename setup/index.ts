import createAssetFragment from "./createAssetFragment";
import createComponentBlocks from "./createComponentBlocks";
import createEntryQueries from "./createEntryQueries";
import createGlobalQueries from "./createGlobalQueries";
import createLinkFragment from "./createLinkFragment";
import createMatrixFragments from "./createMatrixFragments";
import createSuperTableFragments from "./createSuperTableFragments";
import getSchemaData, { SchemaType } from "./getSchemaData";
import { arg } from "./util";

const entryOverrides = arg("entryOverrides");
const additionalGlobal = arg("additionalGlobal");
const queriesPath = arg("queriesPath") || "./web/queries";
const componentsPath = arg("componentsPath") || "./web/components";
const sliceBlocksName = arg("sliceBlocksName") || "componentBlocks";

export type CmdParams = {
  entryOverrides?: string | undefined;
  additionalGlobal?: string | undefined;
  queriesPath: string;
  componentsPath: string;
  sliceBlocksName: string;
};

export type CmdFnParams = CmdParams & { types: SchemaType[] };

async function main(params: CmdParams) {
  const types = await getSchemaData();
  const fnParams = { ...params, types };

  await createEntryQueries(fnParams);
  await createMatrixFragments(fnParams);
  await createSuperTableFragments(fnParams);
  await createAssetFragment(fnParams);
  await createGlobalQueries(fnParams);
  await createComponentBlocks(fnParams);
  await createLinkFragment(fnParams);
}

main({
  entryOverrides,
  additionalGlobal,
  queriesPath,
  componentsPath,
  sliceBlocksName
});
