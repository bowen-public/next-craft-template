import fs from "fs";
import { CmdFnParams } from ".";
import {
  assetFieldsName,
  capitalizeFirst,
  isAssetObject,
  parseAssetObject,
  writeFields
} from "./util";

export default async function createAssetFragment(params: CmdFnParams) {
  const { types } = params;
  const [frag] = types.filter(isAssetObject);
  let fc = "";

  if (frag) {
    const {
      fullName,
      components: [blockName]
    } = parseAssetObject(frag);
    const { fields } = frag;
    if (fields && fields.length) {
      fc += `# ${capitalizeFirst(blockName)} Asset Interface\n\n`;
      fc += `fragment ${assetFieldsName} on ${fullName} {
  ${writeFields(params, fields, "Asset", 2)}
}\n`;
    }
  }

  fs.writeFileSync(`${params.queriesPath}/_assetFragment.graphql`, fc);
}
