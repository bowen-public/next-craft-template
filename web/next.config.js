const {
  PHASE_PRODUCTION_BUILD,
  PHASE_PRODUCTION_SERVER,
  PHASE_DEVELOPMENT_SERVER
} = require("next/constants");

const assetUrl = process.env.NEXT_PUBLIC_ASSET_URL;
const assetPrefix = assetUrl || undefined;
const images = {
  deviceSizes: [640, 750, 828, 1080, 1200, 1920, 2048, 3840],
  imageSizes: [16, 32, 48, 64, 96, 128, 256, 384],
  domains: [process.env.DO_SPACES_HOST]
};

module.exports = (phase) => {
  return {
    serverRuntimeConfig: {
      phase,
      isRunTime:
        phase === PHASE_DEVELOPMENT_SERVER || phase === PHASE_PRODUCTION_SERVER,
      isBuildTime: phase === PHASE_PRODUCTION_BUILD,
      images
    },
    images,
    assetPrefix
    // i18n: {
    //   locales: ["en-US", "es"],
    //   defaultLocale: "en-US"
    // }
  };
};
