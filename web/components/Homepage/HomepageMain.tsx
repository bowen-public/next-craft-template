import { FC } from "react";
import styles from "./HomepageMain.module.scss";

const HomepageMain: FC = () => {
  return <div className={styles.homepageMain}></div>;
};

export default HomepageMain;
