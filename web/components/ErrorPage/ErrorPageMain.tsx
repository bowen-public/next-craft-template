import { SiteNames } from "ctx/siteCtx";
import { FC } from "react";
import styles from "./ErrorPageMain.module.scss";

interface ErrorProps {
  statusCode?: number;
  site: SiteNames;
}

const ErrorPageMain: FC<ErrorProps> = (props) => {
  return (
    <div className={styles.errorPageMain}>
      <h1>404 Page Not Found</h1>
    </div>
  );
};

export default ErrorPageMain;
