import { useCachedGlobalQuery } from "hooks/data";
import Script from "next/script";
import { FC } from "react";

const Gtm: FC = () => {
  const { seomatic } = useCachedGlobalQuery();
  const { metaScriptContainer } = seomatic || {};
  const scriptInner = parseScript(metaScriptContainer);
  // const bodyScript = parseBodyScript(metaScriptContainer);

  return (
    <>
      {/* {bodyScript && (
        <div
          style={{ display: "none" }}
          dangerouslySetInnerHTML={{ __html: bodyScript }}
        ></div>
      )} */}
      {scriptInner && <Script id="__gtm__">{scriptInner}</Script>}
    </>
  );
};

export default Gtm;

function parseScript(s: string | null | undefined) {
  try {
    const o = JSON.parse(s ?? "");
    const script = parseScriptFromStr(o.script);

    if (typeof script === "string") {
      if (script.length) return script;
      return null;
    } else {
      throw "the script prop did not get parsed correctly from craft";
    }
  } catch (err) {
    console.warn("trying to add GTM script failed from Gtm component:", err);
    return null;
  }
}

function parseBodyScript(s: string | null | undefined) {
  try {
    const o = JSON.parse(s ?? "");

    if (typeof o.bodyScript === "string") {
      if (o.bodyScript.length) return o.bodyScript as string;
      return null;
    } else {
      throw "the bodyScript prop did not get parsed correctly from craft";
    }
  } catch (err) {
    console.warn(
      "trying to add GTM bodyScript failed from Gtm component:",
      err
    );
    return null;
  }
}

function parseScriptFromStr(s: string) {
  const result = /<script>((.|\n)*?)<\/script>/g.exec(s);

  if (result) return result[1];
  return null;
}
