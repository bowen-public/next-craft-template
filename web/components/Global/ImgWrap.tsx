import { getLoader } from "lib/image";
import Image, { ImageProps } from "next/image";
import { FC } from "react";

// for next 13

export type ImgProps =
  | {
      id?: string | undefined | null;
      url?: string | undefined | null;
      mimeType?: string | undefined | null;
      title?: string | undefined | null;
    }
  | undefined
  | null;

export type ImgWrapProps = Omit<ImageProps, "src" | "loader" | "alt"> & {
  img: ImgProps;
  wrapCn?: string;
};

const ImgWrap: FC<ImgWrapProps> = (props) => {
  const { fill, wrapCn, img, ...rest } = props;
  const url = img?.url;
  const id = img?.id;
  const mimeType = img?.mimeType;
  const alt = img?.title ?? "";

  if (url && id && mimeType) {
    return fill ? (
      <div className={wrapCn}>
        <Image
          {...rest}
          loader={getLoader({ id, url, mimeType })}
          src={url}
          alt={alt}
          fill={fill}
        />
      </div>
    ) : (
      <Image
        {...rest}
        loader={getLoader({ id, url, mimeType })}
        src={url}
        alt={alt}
        fill={fill}
      />
    );
  }
  return null;
};

export default ImgWrap;
