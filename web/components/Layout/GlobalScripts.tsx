import Script from "next/script";
import { FC, useEffect, useRef, useState } from "react";

const grId = process.env.NEXT_PUBLIC_RECAPTCHA_SITE_KEY;

declare global {
  interface Window {
    waited?: boolean;
  }
}

const GlobalScripts: FC<{ wait?: number }> = ({ wait = 0 }) => {
  // waitRef should never change, if it does we don't care since this is only meant to happen once
  const waitRef = useRef(wait);
  // we need this to trigger the component to render after the initial timeout
  const [, setIsReady] = useState(false);
  const ready = typeof window !== "undefined" && window.waited;

  useEffect(() => {
    let timeoutId: NodeJS.Timeout;
    if (!window.waited) {
      timeoutId = setTimeout(() => {
        window.waited = true;
        setIsReady(true);
      }, waitRef.current);
    }
    return () => {
      clearTimeout(timeoutId);
    };
  }, []);

  return (
    <>
      {!!grId && ready && (
        <Script
          id="__gr__"
          src={`https://www.google.com/recaptcha/api.js?render=${grId}`}
        />
      )}
    </>
  );
};

export default GlobalScripts;
