import { addApolloState } from "@bowendev/apollo";
import HomepageMain from "components/Homepage/HomepageMain";
import LayoutMain from "components/Layout/LayoutMain";
import { siteFromLocale } from "ctx/siteCtx";
import { GlobalDocument } from "generated/documentNodes";
import { initializeApollo } from "lib/apollo";
import { getPreviewToken } from "lib/preview";
import { GetStaticProps, NextPage } from "next";

const HomepagePage: NextPage = () => {
  // const {seomatic} = useCachedHomepageQuery()

  return (
    <LayoutMain
    // seoMarkup={seomatic}
    >
      <HomepageMain />
    </LayoutMain>
  );
};

export default HomepagePage;

export const getStaticProps: GetStaticProps = async (ctx) => {
  const token = getPreviewToken(ctx.previewData);
  const site = siteFromLocale(ctx.locale);
  const apolloClient = initializeApollo(null, { token });
  const context = ctx.preview ? {} : { clientName: "cacheable" };
  // const [, pageRes] =
  await Promise.all([
    apolloClient.query({
      query: GlobalDocument,
      context,
      variables: {}
    })
    // apolloClient.query({
    //   query: HomepageDocument,
    //   context,
    //   variables: {}
    // })
  ]);
  // const [singleEntry] = pageRes.data.homepageEntries || [];

  return addApolloState(apolloClient, {
    // notFound: !singleEntry,
    props: { site },
    revalidate: 60 * 3
  });
};
