import { addApolloState } from "@bowendev/apollo";
import ErrorPageMain from "components/ErrorPage/ErrorPageMain";
import Layout from "components/Layout/LayoutMain";
import { siteFromLocale, SiteNames } from "ctx/siteCtx";
import { GlobalDocument, RedirectDocument } from "generated/documentNodes";
import { initializeApollo } from "lib/apollo";
import { NextPage, NextPageContext } from "next";

interface ErrorProps {
  statusCode?: number;
  site: SiteNames;
}

const ErrorNextPage: NextPage<ErrorProps> = (props) => {
  return (
    <Layout>
      <ErrorPageMain {...props} />
    </Layout>
  );
};

export default ErrorNextPage;

ErrorNextPage.getInitialProps = async (
  ctx: NextPageContext
): Promise<ErrorProps> => {
  const { res, err } = ctx;
  const site = siteFromLocale(ctx.locale);
  const apolloClient = initializeApollo();
  const statusCode = res ? res.statusCode : err ? err.statusCode : 404;
  const uri = ctx.asPath;
  const redirectRes = await apolloClient.query({
    query: RedirectDocument,
    variables: { uri }
  });
  const redirect =
    redirectRes.data && redirectRes.data.retour
      ? redirectRes.data.retour
      : null;

  if (
    redirect &&
    typeof redirect.redirectHttpCode === "number" &&
    typeof redirect.redirectDestUrl === "string"
  ) {
    if (ctx.res) {
      ctx.res.writeHead(redirect.redirectHttpCode, {
        Location: redirect.redirectDestUrl
      });
      ctx.res.end();
    } else {
      window.location.reload();
    }

    return { site };
  }

  await apolloClient.query({
    query: GlobalDocument,
    context: { clientName: "cacheable" },
    variables: {}
  });

  const state = addApolloState(apolloClient, {
    props: {
      statusCode,
      site
    }
  }) as { props: { statusCode: number; site: SiteNames } };

  return state.props;
};
