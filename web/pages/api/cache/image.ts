import {
  ApiCacheImageQuery,
  getTxBuffer,
  arrayBufferToBuffer
} from "@bowendev/image";
import { initializeApollo } from "lib/apollo";
import { NextApiHandler } from "next";

const maxAge = 31536000;

const apiHandler: NextApiHandler = async (req, res) => {
  const apolloClient = initializeApollo();
  const apiImageQuery = req.query as unknown as ApiCacheImageQuery;
  const arrayBuffer = await getTxBuffer({
    apiImageQuery,
    apolloClient
  });
  const buffer = arrayBufferToBuffer(arrayBuffer);

  res.writeHead(200, {
    "Content-Type": apiImageQuery.mimeType,
    "Cache-Control": `max-age=${maxAge}`
  });

  res.end(buffer);
};

export default apiHandler;
