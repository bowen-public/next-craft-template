import { arrayBufferToBuffer, handleImageReq } from "@bowendev/image";
import { getNextCacheUrl } from "lib/url";
import { NextApiHandler } from "next";

const nextCacheUrl = getNextCacheUrl();

const apiHandler: NextApiHandler = async (req, res) => {
  const cachePath = handleImageReq(req, "/api/cache/image");
  const fetchRes = await fetch(nextCacheUrl + cachePath);
  const ab = await fetchRes.arrayBuffer();

  res.writeHead(200, {
    "Content-Type": fetchRes.headers.get("content-type") as string,
    "Cache-Control": fetchRes.headers.get("cache-control") as string
  });
  res.end(arrayBufferToBuffer(ab));
};

export default apiHandler;
