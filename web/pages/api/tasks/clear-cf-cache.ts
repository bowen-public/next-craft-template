import { NextApiHandler } from "next";

const zoneId = process.env.CF_ZONE_ID;
const xAuthEmail = process.env.CF_AUTH_EMAIL;
const xAuthKey = process.env.CF_GLOBAL_API_KEY;
const authorization = process.env.CF_API_KEY;
const cfUrl = `https://api.cloudflare.com/client/v4/zones/${zoneId}/purge_cache`;
let cfHeaders: Record<string, string>;

if (xAuthEmail && xAuthKey && !authorization) {
  cfHeaders = {
    "X-Auth-Email": xAuthEmail,
    "X-Auth-Key": xAuthKey
  };
}

if (xAuthEmail && !xAuthKey && authorization) {
  cfHeaders = {
    "X-Auth-Email": xAuthEmail,
    Authorization: `Bearer ${authorization}`
  };
}

const apiHandler: NextApiHandler = async (req, res) => {
  if (req.headers.auth !== process.env.CRAFT_PREVIEW_SECRET) {
    return res.status(401).json({ message: "Invalid Auth" });
  }
  if (req.headers.env !== "production") {
    return res.status(200).json({ message: "not in prod. doing nothing." });
  }
  clear();

  res.status(200).json({ message: "success" });
};

export default apiHandler;

async function clear(): Promise<void> {
  if (!cfHeaders) {
    console.error("one of CF_GLOBAL_API_KEY or CF_API_KEY needs to be set");
    return;
  }
  if (authorization && xAuthKey) {
    console.error(
      "you have set both CF_GLOBAL_API_KEY and CF_API_KEY. Only one of them should be set"
    );
    return;
  }
  try {
    const cfRes = await fetch(cfUrl, {
      method: "POST",
      body: JSON.stringify({ purge_everything: true }),
      headers: cfHeaders
    });
    const cfData = await cfRes.json();
    console.log(cfData);
  } catch (err) {
    console.log(err);
  }
}
