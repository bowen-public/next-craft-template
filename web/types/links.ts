export interface LinkSet {
  id?: string;
  label: string | null;
  path: string | null;
  entry: SingleEntry;
}

export interface Entry {
  id: string;
  uri: string;
  title?: string;
}

export interface Link {
  label: string | null;
  path: string | null;
}

export type SingleEntry = [Entry] | [];

export type MultiEntries = Entry[];

export type SingleLink = [LinkSet] | [];

export type MultiLinks = LinkSet[];

export type LinkList = Link[];
