import { QueryResult } from "@apollo/client";
import { Dispatch, SetStateAction } from "react";

export type NonNullProps<T> = {
  [K in keyof T]: NonNullable<T[K]>;
};

export type NonNullAndDefinedProps<T> = Required<NonNullProps<T>>;

export type UseStateSetter<T> = Dispatch<SetStateAction<T>>;

export type ExcludeUndefinedAndNull<T> = Exclude<Exclude<T, undefined>, null>;

export type QData<QR extends QueryResult<any, any>> = ExcludeUndefinedAndNull<
  QR["data"]
>;

export type QDataProp<
  QR extends QueryResult<any, any>,
  P extends string
> = QData<QR>[P];

export type DefinedQDataProp<
  QR extends QueryResult<any, any>,
  P extends string
> = ExcludeUndefinedAndNull<QDataProp<QR, P>>;

export type DefinedQDataSingle<
  QR extends QueryResult<any, any>,
  P extends string
> = ExcludeUndefinedAndNull<DefinedQDataProp<QR, P>[0]>;
