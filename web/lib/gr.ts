const grId = process.env.NEXT_PUBLIC_RECAPTCHA_SITE_KEY as string;
const grSecret = process.env.RECAPTCHA_SECRET_KEY as string;

export function getGrToken(action: string): Promise<string | null> {
  return new Promise((resolve) => {
    if (typeof window.grecaptcha === "undefined") {
      console.warn("window.grecaptcha is not defined");
      resolve(null);
      return;
    }
    if (typeof window !== "undefined" && grId) {
      try {
        grecaptcha.ready(() => {
          grecaptcha.execute(grId, { action }).then((token) => {
            resolve(token);
          });
        });
      } catch (err) {
        console.error(err);
        resolve(null);
      }
    } else {
      console.warn(
        "either there is no recaptcha site id set as node env for NEXT_PUBLIC_RECAPTCHA_SITE_KEY, or this is being run on the server"
      );
      resolve(null);
    }
  });
}

export async function getGrServerRes(token: string | null | undefined) {
  if (token) {
    const recaptchaRes = await fetch(
      `https://www.google.com/recaptcha/api/siteverify?secret=${grSecret}&response=${token}`,
      { method: "POST" }
    );

    if (recaptchaRes.ok) {
      const res = await recaptchaRes.json();
      return res as { score: number; success: boolean };
    }
  }

  return null;
}
