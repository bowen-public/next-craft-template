import { PreviewData } from "next";

interface BmPreviewData {
  token?: string;
}

export const getPreviewToken = (
  previewData: PreviewData
): string | undefined => {
  if (typeof previewData === "object") {
    const data = previewData as BmPreviewData;
    return data.token;
  }
  return undefined;
};
