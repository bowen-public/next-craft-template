import parse from "html-react-parser";
import { AnchorHTMLAttributes, FC } from "react";

export * from "./FlexLink";

export interface ExtLinkProps extends AnchorHTMLAttributes<HTMLAnchorElement> {
  newTab?: boolean;
}

export const ExtLink: FC<ExtLinkProps> = (props) => {
  const { href = "#", children, newTab = true, ...rest } = props;
  const atts = newTab
    ? { ...rest, target: "_blank", rel: "noopener noreferrer" }
    : { ...rest };

  return (
    <a href={href} {...atts}>
      {children}
    </a>
  );
};

export interface PhoneLinkProps
  extends AnchorHTMLAttributes<HTMLAnchorElement> {
  number?: string;
}

export const PhoneLink: FC<PhoneLinkProps> = ({
  number = "",
  children,
  ...rest
}) => {
  const href = `tel:${number}`;

  return (
    <a href={href} {...rest}>
      {children ? children : number}
    </a>
  );
};

export interface EmailLinkProps
  extends AnchorHTMLAttributes<HTMLAnchorElement> {
  email?: string;
}

export const EmailLink: FC<EmailLinkProps> = ({
  email = "",
  children,
  ...rest
}) => {
  const href = `mailto:${email}`;

  return (
    <a href={href} {...rest}>
      {children ? children : email}
    </a>
  );
};

export interface GoogleAddressLinkProps
  extends AnchorHTMLAttributes<HTMLAnchorElement> {
  address: string;
}

export const GoogleAddressLink: FC<GoogleAddressLinkProps> = ({
  address,
  children
}) => {
  const href = `${googleQuery}${encodeURIComponent(stripTags(address))}`;

  return <ExtLink href={href}>{children ? children : parse(address)}</ExtLink>;
};

const stripTags = (str: string): string => str.replace(/(<([^>]+)>)/gi, " ");
const googleQuery = "https://www.google.com/maps/search/?api=1&query=";
