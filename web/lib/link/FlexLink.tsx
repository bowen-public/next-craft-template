import { default as NextLink } from "next/link";
import { AnchorHTMLAttributes, FC, ReactNode } from "react";

// for nextjs 13

type AnchorAtt = AnchorHTMLAttributes<HTMLAnchorElement>;

export type LinkSet = {
  path?: string | null | undefined;
  label?: string | null | undefined | ReactNode;
  entry: ({
    id?: string | null | undefined;
    title?: string | null | undefined;
    uri?: string | null | undefined;
  } | null)[];
} | null;

export type FlexLinkProps = AnchorAtt & {
  linkSet: LinkSet;
};

export const FlexLink: FC<FlexLinkProps> = (props) => {
  const { linkSet, children, ...rest } = props;

  if (linkSet) {
    const { path, label, entry } = linkSet;

    if (entry[0]) {
      const uri = entry[0].uri ?? "";
      const l = label ?? entry[0].title;

      return (
        <NextLink href={`/${uri === "__home__" ? "" : uri}`} {...rest}>
          {children ?? l}
        </NextLink>
      );
    }
    if (path) {
      const p =
        path.startsWith("http") || path.startsWith("//")
          ? {
              target: "_blank",
              rel: "noreferrer"
            }
          : {};
      return (
        <NextLink href={path} {...p} {...rest}>
          {children ?? label}
        </NextLink>
      );
    }
  }

  return null;
};
