import { nextCraftSpacesImage } from "@bowendev/image";

export const { getLoader } = nextCraftSpacesImage({
  apiPath: "/api/image"
});
