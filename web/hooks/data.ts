import * as gen from "generated/queries";
import { DefinedQDataSingle, QData } from "types";

// The hooks here are for asserting we know certain things are defined
// For example, since we are using getStaticProps, we know for a fact data is always defined
// Sometimes we can also assert that a certain prop is defined
// For example, if the data is undefined for a single page we are returning notFound
// in the getStaticProps return. This means we know the hook will never run unless the prop is defined

export function useCachedGlobalQuery() {
  const res = gen.useGlobalQuery();

  return res.data as QData<typeof res>;
}

// the below is just an example
// export function useCachedHomepageQuery() {
//   const res = gen.useHomepageQuery();
//   const [data] = res.data?.homepageEntries || [];

//   return data as DefinedQDataSingle<typeof res, "homepageEntries">;
// }
