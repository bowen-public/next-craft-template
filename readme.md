# ${PROJECT_SLUG}

## Initial project setup

1. `npm run localenv` (make sure to export BM_AUTH_OPS_KEY before). This command will setup all local env files, and also run `npm ci` for you in the root and in the web folder.
2. check the untracked files `web/.env.local` and `api/.env` to make sure the variables are filled in
3. Make sure docker desktop is running. (If not on linux)
4. `npm run dev-craft` (wait for everything to be fired up before moving on below)
5. `npm run migrate-prod` (in another terminal while the above command is running) This will pull down the production database to your local setup.
6. `npm run craft-sync` This command will read the craft project config files and sync up everything.
7. Shut down the process in the terminal that you ran `npm run dev-craft` in.
8. Follow the below steps for Dev

## Dev

1. `npm run dev-craft-codegen` (or `npm run dev-craft` if codegen is not setup for the project) craft is now running on localhost:2000. the admin dashboard is in `http://localhost:2000/admin/login`. The username is `admin` password is on the untracked `api/.env` file called `CRAFT_ADMIN_PASSWORD`. The `dev-craft-codegen` as opposed to the `npm run dev-craft` command will run codegen concurrently in the same terminal process. Codegen will handle auto generating types based on the graphql schema.
2. Generally speaking it is advisable to always run `npm run migrate-prod` and `npm run craft-sync` before starting dev especially if you merge some other commits into your branch. (Ignore this if you have already followed `Initial project setup`)
3. If there have been new node modules installed you may need to run `npm run localenv` again. Even though likely no env variables have changed, this will make sure the node_modules are up to date with the package lock files in both the root and in `web`. (Ignore this if you have already followed `Initial project setup`)
4. `npm run dev` in another terminal, nextjs is now running on localhost:3000

## Notes

1. Whenever you change something in craft CMS settings such as a field update, you must manually run `npm run schema` update the schema that is placed in `web/schema.graphql`. Once you run that command codegen will run again automatically and codegen will also run automatically anytime you are writing queries in the `web` folder. It is ONLY needed to run the schema command to tell codegen that something in the graphql schema has changed.
